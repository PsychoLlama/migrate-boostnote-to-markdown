#!/usr/bin/env node
const { createCli } = require('dispute');
const migrate = require('./migrate');

const cli = createCli({
  packageJson: require('../package.json'),
  commandName: 'migrate',
  cli: {
    command: migrate,
    args: '<boostnote-directory>',
  },
});

cli.execute();
