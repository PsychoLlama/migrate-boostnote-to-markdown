const { ExitCode } = require('dispute');
const CSON = require('cson');
const YAML = require('yaml');
const fs = require('fs/promises');
const path = require('path');
const assert = require('assert');

module.exports = async function migrate(options, rootDirectory) {
  const notes = await findUnconvertedNotes(rootDirectory);

  await Promise.all(notes.map(convertFile));
};

async function findUnconvertedNotes(boostnoteDirectory) {
  const notesDirectory = path.join(boostnoteDirectory, 'notes');
  const notebookFilePath = path.join(boostnoteDirectory, 'boostnote.json');
  const allFiles = await fs.readdir(notesDirectory);
  const notebook = JSON.parse(await fs.readFile(notebookFilePath, 'utf8'));

  return allFiles.filter(isBoostnoteFile).map((filename) => ({
    path: path.join(notesDirectory, filename),
    notebook,
  }));
}

function isBoostnoteFile(filename) {
  return /\.cson$/.test(filename);
}

async function convertFile(note) {
  const markdown = await convertToMarkdown(note);

  const { name: noteId } = path.parse(note.path);
  const directory = getAppropriateDirectory(markdown, note);
  const newNoteFile = path.join(directory, `${noteId}.md`);

  await fs.mkdir(directory, { recursive: true });
  await fs.writeFile(newNoteFile, markdown.content);
  await fs.unlink(note.path);
}

// Look in `boostnote.json` to figure out what folder it should go in.
// Boostnote folders are "virtual" (no real directory on the file system). The
// migration script creates the analog. "Default" is a special directory
// that's treated as the root.
function getAppropriateDirectory(markdown, note) {
  const { dir } = path.parse(note.path);
  const folder = note.notebook.folders.find(
    (folder) => folder.key === markdown.folder
  );

  assert(folder, `Cannot find folder name: ${markdown.folder}`);

  if (folder.name === 'default') {
    return dir;
  }

  return path.join(dir, folder.name);
}

async function convertToMarkdown(note) {
  const fileContents = await fs.readFile(note.path, 'utf8');

  // Strip out the metadata. I never used folders or text snippets.
  const {
    content,
    type,
    folder,
    linesHighlighted,
    isStarred,
    isTrashed,
    tags,
    ...metadata
  } = CSON.parse(fileContents);

  // I'm not ready to delete all the notes that were marked trash. I expect
  // they're garbage, but there are quite a few and I'm hesitant to delete
  // them all without checking more closely.
  if (isTrashed) {
    metadata.isTrashed = true;
  }

  if (tags.length) {
    metadata.tags = tags;
  }

  const frontmatter = `---\n${YAML.stringify(metadata)}---\n`;
  return {
    content: `${frontmatter}\n${content}`,
    folder,
  };
}
